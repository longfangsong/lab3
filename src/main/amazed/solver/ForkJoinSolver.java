package amazed.solver;

import amazed.maze.Maze;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.atomic.AtomicBoolean;

class Pair<L, R> {
    public final L fst;
    public final R snd;

    public Pair(L left, R right) {
        this.fst = left;
        this.snd = right;
    }
}

/**
 * <code>ForkJoinSolver</code> implements a solver for
 * <code>Maze</code> objects using a fork/join multi-thread
 * depth-first search.
 * <p>
 * Instances of <code>ForkJoinSolver</code> should be run by a
 * <code>ForkJoinPool</code> object.
 */

public class ForkJoinSolver
        extends SequentialSolver {
    protected ConcurrentSkipListSet<Integer> visited;

    protected List<Integer> toVisitList;

    protected AtomicBoolean found;

    /**
     * Creates a solver that searches in <code>maze</code> from the
     * start node to a goal.
     *
     * @param maze the maze to be searched
     */
    public ForkJoinSolver(Maze maze, int forkAfter) {
        super(maze);
        this.forkAfter = forkAfter;
        this.found = new AtomicBoolean(false);
    }

    @Override
    protected void initStructures() {
        super.initStructures();
        this.visited = new ConcurrentSkipListSet<>();
        this.toVisitList = new ArrayList<Integer>();
        this.toVisitList.add(maze.start());
        this.predecessor = new ConcurrentHashMap<Integer, Integer>();
        this.found = new AtomicBoolean(false);
    }

    /**
     * Creates a solver that searches in <code>maze</code> from the
     * start node to a goal, forking after a given number of visited
     * nodes.
     *
     * @param maze      the maze to be searched
     * @param forkAfter the number of steps (visited nodes) after
     *                  which a parallel task is forked; if
     *                  <code>forkAfter &lt;= 0</code> the solver never
     *                  forks new tasks
     */
    public ForkJoinSolver(Maze maze, int forkAfter, ConcurrentSkipListSet<Integer> visited,
            List<Integer> toVisitList, Map<Integer, Integer> predecessor) {
        super(maze);
        this.forkAfter = forkAfter;
        this.visited = visited;
        this.toVisitList = toVisitList;
        this.predecessor = predecessor;
        this.found = new AtomicBoolean(false);
        if (this.visited == null)
            this.visited = new ConcurrentSkipListSet<>();
        if (this.toVisitList == null) {
            this.toVisitList = new ArrayList<>();
            this.toVisitList.add(maze.start());
        }
        if (this.predecessor == null)
            this.predecessor = new ConcurrentHashMap<Integer, Integer>();
    }

    /**
     * Searches for and returns the path, as a list of node
     * identifiers, that goes from the start node to a goal node in
     * the maze. If such a path cannot be found (because there are no
     * goals, or all goals are unreacheable), the method returns
     * <code>null</code>.
     *
     * @return the list of node identifiers from the start node to a
     *         goal node in the maze; <code>null</code> if such a path cannot
     *         be found.
     */
    @Override
    public List<Integer> compute() {
        return parallelSearch();
    }

    private List<Integer> parallelSearch() {
        Integer player = null;
        while (this.toVisitList.size() < this.forkAfter && this.toVisitList.size() != 0) {
            int current = toVisitList.remove(toVisitList.size() - 1);
            if (!visited.contains(current)) {
                if (player == null) {
                    player = maze.newPlayer(current);
                }
                visited.add(current);
                maze.move(player, current);
                if (maze.hasGoal(current)) {
                    maze.remove(player);
                    // TODO: not necessary unless it's top level
                    var result = new ArrayList<Integer>();
                    result.add(current);
                    this.found.set(true);
                    return result;
                } else {
                    var neighbors = maze.neighbors(current);
                    for (int nb : neighbors) {
                        if (!visited.contains(nb)) {
                            toVisitList.add(nb);
                            predecessor.put(nb, current);
                        }
                    }
                }
            }
        }
        if (player != null)
            maze.remove(player);
        if (this.found.get()) {
            return null;
        }
        if (this.toVisitList.size() != 0) {
            var firstPart = new ArrayList<>(this.toVisitList.subList(0, this.toVisitList.size() / 2));
            var secondPart = new ArrayList<>(
                    this.toVisitList.subList(this.toVisitList.size() / 2, this.toVisitList.size()));
            var firstSolver = new ForkJoinSolver(this.maze, this.forkAfter, this.visited, firstPart, this.predecessor);
            var secondSolver = new ForkJoinSolver(maze, this.forkAfter, this.visited, secondPart, this.predecessor);
            var firstTask = firstSolver.fork();
            var secondTask = secondSolver.fork();
            var firstResult = firstTask.join();
            var secondResult = secondTask.join();
            if (firstResult != null) {
                return pathFromTo(start, firstResult.get(firstResult.size() - 1));
            }
            if (secondResult != null) {
                return pathFromTo(start, secondResult.get(secondResult.size() - 1));
            }
        }
        return null;
    }
}
